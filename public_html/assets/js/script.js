$(function () {



    var products = [],
            cart = {},
            filters = {};
            

    //	Event handlers for frontend navigation

    //	Checkbox filtering
    var counter = 0;
    var checkboxes = $('.all-products input[type=checkbox]');
    var stores = [];
    var deletebuttons = $('.all-products button[class=deleteCart]');
    
    deletebuttons.click(function (){
        console.log("siinnna");
        var that = $(this),
                buttonId = that.attr('id');
        console.log(that);
        console.log(buttonId);
    });
    checkboxes.click(function () {
        
        var that = $(this),
                specName = that.attr('name');

        if (that.is(":checked")) {


            if (!(filters[specName] && filters[specName].length)) {
                filters[specName] = [];
            }


            filters[specName].push(that.val());


            createQueryHash(filters);

        }

        // When a checkbox is unchecked we need to remove its value from the filters object.
        if (!that.is(":checked")) {

            if (filters[specName] && filters[specName].length && (filters[specName].indexOf(that.val()) != -1)) {


                var index = filters[specName].indexOf(that.val());


                filters[specName].splice(index, 1);


                if (!filters[specName].length) {
                    delete filters[specName];
                }

            }


            createQueryHash(filters);
        }
    });

    // When the "Clear all filters" button is pressed change the hash to '#' (go to the home page)
    $('.filters button').click(function (e) {
        e.preventDefault();
        window.location.hash = '#';
    });


    var singleProductPage = $('.single-product');

    singleProductPage.on('click', function (e) {

        if (singleProductPage.hasClass('visible')) {

            var clicked = $(e.target);

            if (clicked.hasClass('close') || clicked.hasClass('overlay')) {
                createQueryHash(filters);
            }

        }

    });




    // Get data about our products 
    $.getJSON("https://erply-challenge.herokuapp.com/list/?AUTH=fae7b9f6-6363-45a1-a9c9-3def2dae206d", function (data) {

        // Write the data into our global variable.
        products = data;

        generateAllProductsHTML(products);

        $(window).trigger('hashchange');

    });




    $(window).on('hashchange', function () {
        render(decodeURI(window.location.hash));
    });


    // Navigation

    function render(url) {

        var temp = url.split('/')[0];

        $('.main-content .page').removeClass('visible');


        var map = {

            // The "Homepage".
            '': function () {

                filters = {};
                checkboxes.prop('checked', false);

                renderProductsPage(products);
            },

            // Single Products page.
            '#product': function () {

                var index = url.split('#product/')[1].trim();

                renderSingleProductPage(index, products);
            },

            // Page with filtered products
            '#filter': function () {

                url = url.split('#filter/')[1].trim();

                try {
                    filters = JSON.parse(url);
                }
                catch (err) {
                    window.location.hash = '#';
                    return;
                }

                renderFilterResults(filters, products);
            }

        };

        if (map[temp]) {
            map[temp]();
        }
        else {
            console.log("midagi on pekkis");
        }

    }



    function generateAllProductsHTML(data) {

        var list = $('.all-products .products-list');

        var theTemplateScript = $("#products-template").html();
        //Compile the template​
        var theTemplate = Handlebars.compile(theTemplateScript);
        list.append(theTemplate(data));


        list.find('li').on('click', function (e) {
            e.preventDefault();

            var productIndex = $(this).data('index');

            window.location.hash = 'product/' + productIndex;
        })
    }

    function renderProductsPage(data) {

        var page = $('.all-products'),
                allProducts = $('.all-products .products-list > li');

        allProducts.addClass('hidden');

        console.log("siin");
        allProducts.each(function () {

            var that = $(this);

            data.forEach(function (item) {

                var found = false;
                for (var i = 0; i < stores.length; i++) {
                    if (stores[i].store == item.store) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    var element = {"store": item.store};
                    stores.push(element);
                    $('#storesFilter').append('<label><input type="checkbox" name="store" value="'+ item.store +'">'+ item.store +'</label>');
                }
                if (that.data('index') == item.id) {
                    that.removeClass('hidden');
                }
            });
        });
        
        var list = $('.all-products .products-list');
        /*
        console.log(stores);
        var template = $("#stores-template").html();
        var theTemplate = Handlebars.compile(template);
        document.getElementById("storesFilter").innerHTML += stores;*/
        //list.append(theTemplate(stores));

        page.addClass('visible');
        checkboxes = $('.all-products input[type=checkbox]');

    }
    
    // Adds item to cart
    function addToCart(item) {
        cart[counter] = item;       
         var current_counter = counter;
        $("#shoppingCart").append('<li id ="'+ counter +'"><span>' + item.name + ' ' + item.price + ' ' +item.currency + '<button style = "padding: 5px 5px 15px 15px" class = "deleteCart" id = "'+ counter +'"> <i class="fa fa-close"></i></button></span></li>');
        deletebuttons = $('.all-products button[class=deleteCart]');
        console.log(deletebuttons);
        document.getElementById(counter).addEventListener("click",function(e) {
            if(deleteFromCart(current_counter)){
                $('#'+current_counter).remove();
                console.log("peaks kustutama");
            }
        });
        counter++;
    }
    
    function deleteFromCart(licounter) {
        for (var k in cart) {
            if (cart.hasOwnProperty(k)) {
                if (licounter.toString() === k) {
                    delete cart[licounter];                   
                    return true;
                }
            }
        }
        /*
         cart.forEach(function (item) {
         if (item.id == itemId) {
         cart.pop(item);
         $("#" + item.id).remove();
         }
         });*/

    }

    // Opens up a preview for one of the products.
    // Its parameters are an index from the hash and the products object.
    function renderSingleProductPage(index, data) {

        var page = $('.single-product'),
                container = $('.preview-large');
        var current_item;
        if (data.length) {
            data.forEach(function (item) {
                if (item.id == index) {
                    current_item = item;
                    // Populate '.preview-large' with the chosen product's data.
                    $('#name').text(item.name);
                    container.find('img').attr('src', item.image);
                    $('#desc').text(item.description);
                    $('#price').text(item.price + " " + item.currency);
                    if (item.instock) {
                        $('#stock').text("In stock");
                    } else {
                        $('#stock').text("Not in stock");
                    }
                    $('#store').text(item.store);
                    $('#code').text(item.productcode);
                }
            });
        }
        $("#addCart").on('click', function (e) {

            addToCart(current_item);

        });
        page.addClass('visible');

    }

    function renderFilterResults(filters, products) {

        var criteria = ['instock', 'store', 'camera'],
                results = [],
                isFiltered = false;

        checkboxes.prop('checked', false);

        criteria.forEach(function (c) {
            if (filters[c] && filters[c].length) {


                if (isFiltered) {
                    products = results;
                    results = [];
                }
                //console.log(products);

                filters[c].forEach(function (filter) {
                    // Iterate over the products.
                    products.forEach(function (item) {


                        if (typeof item[c] == 'string') {
                            console.log('string');
                            if (item[c].toLowerCase().indexOf(filter) != -1) {
                                results.push(item);
                                isFiltered = true;
                            }
                        }

                        if (typeof item[c] == 'boolean') {
                            if (item[c] === $.parseJSON(filter)) {
                                results.push(item);
                                isFiltered = true;
                            }
                        }

                    });


                    if (c && filter) {
                        $('input[name=' + c + '][value=' + filter + ']').prop('checked', true);
                    }
                });
            }

        });

        renderProductsPage(results);
    }



    function createQueryHash(filters) {

        if (!$.isEmptyObject(filters)) {
            window.location.hash = '#filter/' + JSON.stringify(filters);
        } else {
            window.location.hash = '#';
        }

    }

});